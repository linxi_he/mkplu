#include <iostream>
#include <math.h>
#include <boost/shared_ptr.hpp>
#include <sdf/sdf.hh>
#include <ignition/math/Quaternion.hh>
#include <ignition/math/Vector3.hh>

#include "gazebo/common/Animation.hh"
#include "gazebo/common/CommonTypes.hh"
#include "gazebo/common/KeyFrame.hh"
#include "gazebo/gazebo.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/common/common.hh"
#include "gazebo/msgs/msgs.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"



namespace gazebo
{
  class MsgTestActuator : public ModelPlugin
  {
    typedef const boost::shared_ptr<const gazebo::msgs::Vector3d>
    Vector3dPtr;
   
      
    public: void Load(physics::ModelPtr _parent, sdf::ElementPtr _sdf)
    {
        this->model = _parent;
        this->connections.push_back(event::Events::ConnectWorldUpdateBegin(
              std::bind(&MsgTestActuator::OnUpdate, this, std::placeholders::_1)));
        
        this->node = transport::NodePtr(new transport::Node()); //为节点分配空间
        this->node->Init(this->model->GetWorld()->Name()); //以modelname作为节点名??

       
        this->Sub =this->node->Subscribe("~/msgs_test/target", &MsgTestActuator::Actuator, this); 
    }

    public: void Actuator(Vector3dPtr &msg)
    {
      std::cout << "subscribe" << std::endl;
      std::cout << "x is: " << msg->x() << std::endl;
      this->pose.Pos().X(msg->x());
      
      //this->SetPose(msg->x());
    }

    /*public: void SetPose(const double &_vel)
    {
     
     this->pose.Set(5,0,0,0,0,0);   
    }
l*/
    public: void OnUpdate(const common::UpdateInfo &_info)
    {
        this->model->SetWorldPose(this->pose,false,false);
    }

    private: ignition::math::Pose3d pose;
    private: physics::ModelPtr model;
    private: transport::NodePtr node;
    private: transport::SubscriberPtr Sub;
    private: std::vector<event::ConnectionPtr> connections;

  };

  // Register this plugin with the simulator
  GZ_REGISTER_MODEL_PLUGIN(MsgTestActuator)
}
