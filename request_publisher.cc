#include <iostream>
#include <math.h>
#include <deque>
#include <sdf/sdf.hh>

#include "gazebo/gazebo.hh"
#include "gazebo/common/common.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/msgs/msgs.hh"

#include <gazebo/gazebo_client.hh>


//#include "collision_map_request.pb.h"
//#include "vector2d.pb.h"

using namespace std;

//将坐标值的字符串"(x,y,z)"转换成vector3d序列
void createVectorArray(const char * vectorString,
                        gazebo::msgs::Vector3d* request) //接受一个字符串，一个2维数组
{
    
    string cornersStr = vectorString;
   
    size_t commaLoc = cornersStr.find(",");
    string x = cornersStr.substr(1,commaLoc);
    size_t commaLoc2 = cornersStr.find(",",commaLoc);
    string y = cornersStr.substr(commaLoc + 1, commaLoc2);
    string z = cornersStr.substr(commaLoc2 + 1, cornersStr.length());

    request->set_x(atof(x.c_str()));
    request->set_y(atof(y.c_str()));
    request->set_z(atof(z.c_str()));    
}

int main(int _argc, char **_argv)  //参数个数，参数值“一个字符串”
{
    gazebo::client::setup(_argc, _argv);


    // Create our node for communication
    gazebo::transport::NodePtr node(new gazebo::transport::Node());
    node->Init();

  // Publish to the  velodyne topic
    gazebo::transport::PublisherPtr pub =
    node->Advertise<gazebo::msgs::Vector3d>("~/msgs_test/target");

    // Wait for a subscriber to connect to this publisher
    pub->WaitForConnection();

    // Create a a vector3 message
    gazebo::msgs::Vector3d msg;

    gazebo::msgs::Set(&msg, ignition::math::Vector3d(std::atof(_argv[1]), 0, 0));

    // Set the velocity in the x-component
    //createVectorArray(_argv[1],msg);
    // Send the message
    pub->Publish(msg);

    // Make sure to shut everything down.

    gazebo::client::shutdown();
//////////////////////////////////////////////////////////////
    
   /* 
   //写入坐标数据
   gazebo::msgs::Vector3d request;

    createVectorArray(argv[1],request);
            
    gazebo::transport::init(); //重要！！初始化gazebo::transport功能

    gazebo::transport::run();  //运行功能
    gazebo::transport::NodePtr node(new gazebo::transport::Node()); //为节点分配空间
        node->Init(); //以"default"为名创建节点


        gazebo::transport::PublisherPtr PosePub =
                node->Advertise<gazebo::msgs::Vector3d>("~/msgs_test/target");
                                                            
        PosePub->WaitForConnection(); //等待connection with master
        PosePub->Publish(request);  //发送msg

        gazebo::transport::fini(); //释放节点空间
        return 0;
   
   */ 
    
    
   
    
}